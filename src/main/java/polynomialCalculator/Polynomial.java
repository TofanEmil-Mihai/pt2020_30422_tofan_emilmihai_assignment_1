package polynomialCalculator;
import java.util.*;
public class Polynomial {
	
	public ArrayList<Monomial> monomialsList;
	
	public Polynomial()
	{
		monomialsList = new ArrayList<Monomial>();
	}
	
	public Polynomial(ArrayList<Monomial> polynomial)
	{
		this.monomialsList = polynomial;
	}
	
	public Polynomial(String stringForm)
	{
		
		this.monomialsList = new ArrayList<Monomial>();
		
		
		String replacementPlusMinus = stringForm.replaceAll("-" , "+-");
		String [] monomials = replacementPlusMinus.split("\\+");
		
		double coefficient = 0;
		int power = 0;
		
		for(String i:monomials) 
		{
			if(!i.equals(""))
			{
				String[] coefAndExp = i.split("x\\^");
			
			
			for(String j: coefAndExp)
			{
				if(j.contains("x"))
				{
					String[] components = j.split("x");
					if(components.length == 1 && components[0].equals("-"))
					{	
						//termen de forma ax
						coefficient = -1;
						power = 1;
					}
					else if(components.length == 0) {
						//termen de forma x
						coefficient = 1;
						power = 1;
					}
					else {
						//termen de forma -x
						coefficient =  Integer.parseInt(components[0]);
						power = 1;
					}
				}
				else if(coefAndExp.length == 1){
					//termen liber
					coefficient = Integer.parseInt(coefAndExp[0]);
					power = 0;
					
				}else if(coefAndExp.length == 2 && coefAndExp[0].equals("")) {
					coefficient = 1;
					power = Integer.parseInt(coefAndExp[1]);
						
				}
				else if(coefAndExp.length == 2 && coefAndExp[0].equals("-")) {
					coefficient = -1;
					power = Integer.parseInt(coefAndExp[1]);
				}
				else
				{
					coefficient = Integer.parseInt(coefAndExp[0]);
					power = Integer.parseInt(coefAndExp[1]);
				}
				
				
			}
			}
				
				Monomial addMonomial = new Monomial(coefficient, power);
			   this.monomialsList.add(addMonomial);
				
		}
					
		}
			
	
	public ArrayList<Monomial> getMonomials()
	{
		return this.monomialsList;
		
	}
	
	public Polynomial correctPolynomial()
	{
		
	    Polynomial resultPolynomial = new Polynomial();
	    
	    for(Monomial monomial1 : this.getMonomials())
	    {
	    	if(monomial1.getCoefficient() != 0)
	    		resultPolynomial.getMonomials().add(monomial1);
	    		
	    }
	    
	    
	    resultPolynomial.getMonomials().sort(new Comparator<Monomial>()
	    {
	    	public int compare(Monomial monomial1, Monomial monomial2) {
	    		Integer i = new Integer(monomial2.getPower());
	    		return i.compareTo(monomial1.getPower());
	    	}
	    });
	    
	    return resultPolynomial;
	    
	    
	}
	
	public String toString() {
		String s = "";
		
		for(Monomial i:this.monomialsList) {
			
			if(i != monomialsList.get(monomialsList.size() - 1))
				s += i.toString() + "+";
			else {
				s += i.toString();
			}
		}
		
		return s;
	}

}
