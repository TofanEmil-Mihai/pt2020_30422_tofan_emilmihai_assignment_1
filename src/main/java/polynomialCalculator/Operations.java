package polynomialCalculator;

import java.util.Collections;

public class Operations {
	
	
	
	public Polynomial addition(Polynomial polynomial1,Polynomial polynomial2)
	{
		Polynomial resultPolynomial = new Polynomial();
		Polynomial tempPolynomial = new Polynomial();
		
		for(Monomial monomial1 : polynomial1.getMonomials()  )
		{
			resultPolynomial.getMonomials().add(monomial1);
		}
			
	    for(Monomial monomial2 : polynomial2.getMonomials())
	    {
	    	resultPolynomial.getMonomials().add(monomial2);
	    }
	    for(Monomial monomial1 : polynomial1.getMonomials())
	    {
	    	for(Monomial monomial2 : polynomial2.getMonomials())
	    	{
	    		if(monomial1.getPower() == monomial2.getPower())
	    		{
	    			resultPolynomial.getMonomials().remove(monomial1);
	    			resultPolynomial.getMonomials().remove(monomial2);
	    			
	    			resultPolynomial.getMonomials().add(monomial1.addMonomial(monomial2));
	    		}
	    	}
	    }
	    
	    
		resultPolynomial = resultPolynomial.correctPolynomial();
		return resultPolynomial;
	}
	
	public Polynomial subtraction(Polynomial polynomial1, Polynomial polynomial2)
	{
		Operations O = new Operations();
		Polynomial resultPolynomial = new Polynomial();
		Polynomial tempPolynomial = new Polynomial();
		
		
		for(Monomial monomial1 : polynomial1.getMonomials()  )
		{
			resultPolynomial.getMonomials().add(monomial1);
		}
			
	    for(Monomial monomial2 : polynomial2.getMonomials())
	    {
	    	tempPolynomial.getMonomials().add(new Monomial(-(monomial2.getCoefficient())  ,monomial2.getPower()));
	    }
	    
	    
	   resultPolynomial = O.addition(resultPolynomial, tempPolynomial);
	   
	   resultPolynomial = resultPolynomial.correctPolynomial();
	   return resultPolynomial;
	}
	
	public Polynomial multiplication(Polynomial polynomial1, Polynomial polynomial2)
	{
		Operations O = new Operations();
		Polynomial resultPolynomial = new Polynomial();
		Polynomial tempPolynomial = new Polynomial();
		
		for(Monomial monomial1 : polynomial1.getMonomials())
		{
			tempPolynomial.getMonomials().clear();
			
			for(Monomial monomial2 : polynomial2.getMonomials())
			{
				tempPolynomial.getMonomials().add(monomial1.multiplyMonomial(monomial2));
			}
			
			resultPolynomial = O.addition(resultPolynomial, tempPolynomial);
		}
		
		
		resultPolynomial = resultPolynomial.correctPolynomial();
		return resultPolynomial;
	}
	
	public Polynomial differentiation(Polynomial polynomial1)
	{
		Polynomial resultPolynomial = new Polynomial();
		for(Monomial monomial1 : polynomial1.getMonomials())
		{
			resultPolynomial.getMonomials().add(monomial1.differentiateMonomial());
		}
		
		resultPolynomial = resultPolynomial.correctPolynomial();
		return resultPolynomial;
		
	}
	
	public Polynomial integration(Polynomial polynomial1)
	{
		Polynomial resultPolynomial = new Polynomial();
		
		for(Monomial monomial1 : polynomial1.getMonomials())
		{
			resultPolynomial.getMonomials().add(monomial1.integrateMonomial());
		}
		
		resultPolynomial = resultPolynomial.correctPolynomial();
		return resultPolynomial;
		
	}

	
	public static void main(String args[]) {
		
		Operations calc = new Operations();
		Polynomial a = new Polynomial("x^3-3x^4+5"); 
		Polynomial b = new Polynomial("-x^2-2x^4+2");
		
		
		
		Polynomial r = a.correctPolynomial();
		Polynomial x = b.correctPolynomial();
		
		System.out.println("First Polynomial is : " +r);
		System.out.println("Second Polynomial is : " +x);
		System.out.println();
		
		Polynomial rezAddition = new Polynomial();
		
		
		rezAddition = calc.addition(r,x);
		System.out.println("Addition : " +rezAddition);
		
		
		Polynomial rezSubtraction = new Polynomial();
		rezSubtraction = calc.subtraction(r,x);
		System.out.println("Subtraction : " +rezSubtraction);
		
		
	    Polynomial rezMultiplication = new Polynomial();
		rezMultiplication = calc.multiplication(r,x);
		rezMultiplication = rezMultiplication.correctPolynomial();
		System.out.println("Multiplication : " +rezMultiplication);
		
		
		
		Polynomial rezDifferentiation = new Polynomial();
		rezDifferentiation = calc.differentiation(r);
		System.out.println("Differentiation : " +rezDifferentiation);
		
		Polynomial rezIntegration = new Polynomial();
		rezIntegration = calc.integration(r);
		System.out.println("Integration : " +rezIntegration);
		
		
		
		}

}
