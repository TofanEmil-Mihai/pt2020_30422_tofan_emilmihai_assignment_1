package polynomialCalculator;


public class Monomial implements Comparable {
    
	
	double coefficient;
	int power;
	
	public Monomial()
	{
		
	}
	
	public Monomial(double coefficient, int power)
	{
		this.coefficient = coefficient;
		this.power = power;
	}
	


	public void setCoefficient(double coefficient)
	{
		this.coefficient = coefficient;
	}
	
	public void setPower(int power)
	{
		this.power = power;
	}
	
	public double getCoefficient()
	{
		return this.coefficient;
	}
	
	public int getPower()
	{
		return this.power;
	}
	
	public Monomial addMonomial(Monomial monomial2)
	{
		
		Monomial resultMonomial = new Monomial(this.coefficient + monomial2.coefficient ,this.power);
		 return resultMonomial;
		
	}
	
	public Monomial subtractMonomial(Monomial monomial2)
	{
		Monomial resultMonomial = new Monomial(this.coefficient - monomial2.coefficient, this.power);
		return resultMonomial;
	}
	
	public Monomial multiplyMonomial(Monomial monomial2)
	{
		Monomial resultMonomial = new Monomial(this.coefficient * monomial2.coefficient, this.power + monomial2.power);
		return resultMonomial;
		
	}
	
	public Monomial differentiateMonomial()
	{
		Monomial notConstantMonomial = new Monomial(this.coefficient * this.power, this.power -1);
		Monomial constantMonomial = new Monomial(0, 0);
		Monomial resultMonomial = new Monomial();
		
		if(this.power == 0)
		{
			resultMonomial = constantMonomial;
		}
	   
		else if(this.power > 0)
		{
		    resultMonomial = notConstantMonomial;
		}
		
		return resultMonomial;
		
		
	   
	}
	
	
	public Monomial integrateMonomial()
	{
		Monomial resultMonomial = new Monomial(this.coefficient/(this.power+1), this.power+1);
		return resultMonomial;
	}
	
	public int compareTo(Object o)
	{
		Monomial monomial1 = (Monomial)o;
		
		if(this.power == monomial1.power)
		    return 0;
		
		else if(this.power < monomial1.power)
			return -1;
		else return 1;
		
		
	}
	
	public String toString()
	{
		String s = "";
		
		return s + this.coefficient + "*x^" + this.power;
	}

	
}

	

