package JUnit;
import static org.junit.Assert.*;

import org.junit.*;

import polynomialCalculator.*;

public class JUnitTest {


	    private static Operations c;
	    private static Polynomial a;
	    private static Polynomial b;

	    private static int nOfTests=0;
	    private static int nOfSTests=0;

	    public JUnitTest()
	    {
	        System.out.println("Constructor before testing:");
	    }
	    @BeforeClass
	    public static void setUpBeforeClass() throws Exception {

	        Operations c = new Operations();
	        Polynomial a = new Polynomial();
	        Polynomial b = new Polynomial();
	    }

	    @AfterClass
	    public static void tearDownAfterClass() throws Exception {

	        System.out.println("Number of executed tests: " + nOfTests + "\n "+ "Number of successfully executed tests: " + nOfSTests + "");
	    }

	    @Before
	    public void setUp() throws Exception {
	        System.out.println("New test!");
	        nOfTests++;
	    }

	    @After
	    public void tearDown() throws Exception {
	        System.out.println("Current test finished!");
	    }


	    @Test
	    public void testAdunare() {
	        a = new Polynomial("2x^3-3x^4+5");
	        b = new Polynomial("3x^2-2x^4+2");
	        c = new Operations();

	        Polynomial rez = c.addition(a,b);
            rez = rez.correctPolynomial();
	        assertNotNull(rez.toString());
	        assertEquals(rez.toString(),"-5.0*x^4+2.0*x^3+3.0*x^2+7.0*x^0");
	        nOfSTests++;
	    }
	    @Test
	    public void testScadere() {
	        a = new Polynomial("2x^3-3x^4+5");
	        b = new Polynomial("3x^2-2x^4+2");
	        c = new Operations();

	        Polynomial rez = c.subtraction(a,b);
            rez = rez.correctPolynomial();
	        assertNotNull(rez.toString());
	        assertEquals(rez.toString(),"-1.0*x^4+2.0*x^3+-3.0*x^2+3.0*x^0");
	        nOfSTests++;
	    }

	    @Test
	    public void testInmultire() {

	        a = new Polynomial("x^1+2");
	        b = new Polynomial("x^1+1");
	        c = new Operations();

	        Polynomial rez = c.multiplication(a,b);

	        assertNotNull(rez.toString());
	        assertEquals(rez.toString(),"1.0*x^2+3.0*x^1+2.0*x^0");
	        nOfSTests++;
	    }
	   
	    @Test
	    public void testDerivare() {
	        a = new Polynomial("3x^2+2x");
	        c = new Operations();

	        Polynomial rez = c.differentiation(a);

	        assertNotNull(rez.toString());
	        assertEquals(rez.toString(),"6.0*x^1+2.0*x^0");
	        nOfSTests++;
	    }

	    @Test
	    public void testIntegrare() {

	        a = new Polynomial("3x^2+2x");
	        c = new Operations();

	        Polynomial rez = c.integration(a);

	        assertNotNull(rez.toString());
	        assertEquals(rez.toString(),"1.0*x^3+1.0*x^2");

	        nOfSTests++;
	    }
}

	
